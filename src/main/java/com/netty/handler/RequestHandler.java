package com.netty.handler;

import com.Exceptions.UnsupportedEntity;
import com.entity.Action;
import com.entity.SendEntity;
import com.netty.server.Server;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.handler.codec.http.*;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by Alexander Yakovlev on 19/08/2017.
 */
public class RequestHandler extends MessageToMessageDecoder<DefaultFullHttpRequest> {

    @Override
    protected void decode(ChannelHandlerContext ctx, DefaultFullHttpRequest request, List<Object> out) throws Exception {
        QueryStringDecoder reqUri = new QueryStringDecoder(request.getUri());

        String url = reqUri.path();
        url = url == null ? "" : url.toLowerCase();
        String[] urlParts = url.split("/");

        Action action = null;

        SendEntity sendEntity = getSendEntity(urlParts[1], ctx, request);

        if (request.getMethod() == HttpMethod.GET) {
            action = Action.SELECT;
        } else if (request.getMethod() == HttpMethod.POST) {
            if (urlParts.length == 3) {
                if ("new".equals(urlParts[2])) {
                    action = Action.CREATE;
                } else {
                    action = Action.UPDATE;
                }
            }
        }


        if (sendEntity != null) {
            HttpHeaders headers = request.headers();

            if (action != Action.CREATE && StringUtils.isNumeric(urlParts[2])) {
                headers.add("id", urlParts[2]);
            } else {
                Server.sendError(ctx, "Wrong path", HttpResponseStatus.NOT_FOUND);
            }
            headers.add("action", action);
            headers.add("sendEntity", sendEntity);

            out.add(request);
            request.retain();
        } else {
            Server.sendError(ctx, "No entity", HttpResponseStatus.NOT_FOUND);

        }

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        Server.sendError(ctx, cause.getMessage(), HttpResponseStatus.INTERNAL_SERVER_ERROR);
    }

    private SendEntity getSendEntity(String entityStr, ChannelHandlerContext ctx, DefaultFullHttpRequest request) throws UnsupportedEntity {
        switch (entityStr) {
            case "user":
                return SendEntity.USER;
            case "location":
                return SendEntity.LOCATION;
            case "visit":
                return SendEntity.VISIT;
            case "users":
                return SendEntity.USER_VISITS;
            case "locations":
                return SendEntity.LOCATION_AVG_MARK;
            default:
                Server.sendError(ctx, "Wrong entity", HttpResponseStatus.NOT_FOUND);
                return null;
        }
    }


}
