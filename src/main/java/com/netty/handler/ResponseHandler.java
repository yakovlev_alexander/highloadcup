package com.netty.handler;


import com.Exceptions.IncorrectParameterException;
import com.Exceptions.UnsupportedEntity;
import com.Storage;
import com.entity.Action;
import com.entity.Entity;
import com.entity.SendEntity;
import com.json.JsonConverter;
import com.json.Parser;
import com.netty.server.Server;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Created by Alexander Yakovlev on 19/08/2017.
 */
public class ResponseHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        if (cause instanceof IncorrectParameterException) {
            Server.sendError(ctx, cause.getMessage(), HttpResponseStatus.BAD_REQUEST);
        } else {
            cause.printStackTrace();
            Server.sendError(ctx, cause.getMessage(), HttpResponseStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object obj) throws Exception {
        DefaultFullHttpRequest request = (DefaultFullHttpRequest) obj;
        String content = request.content().toString(CharsetUtil.UTF_8);


        HttpHeaders headers = request.headers();
        Action action = Action.valueOf(headers.get("action"));
        SendEntity sendEntity = SendEntity.valueOf(headers.get("sendEntity"));

        JSONObject outJson = null;
        if (Action.CREATE == action) {
            saveEntity(sendEntity, content);
        } else {
            Long id = Long.parseLong(headers.get("id"));
            if (Action.UPDATE == action) {
                updateEntity(sendEntity, id, content);
            } else if (Action.SELECT == action) {
                QueryStringDecoder reqUri = new QueryStringDecoder(request.getUri());

                outJson = selectEntity(sendEntity, id, reqUri);
            }
        }

        ByteBuf sendContent = Unpooled.copiedBuffer(outJson != null ? outJson.toString() : "{}", StandardCharsets.UTF_16);
        DefaultFullHttpResponse response =
                new DefaultFullHttpResponse(
                        HttpVersion.HTTP_1_1,
                        HttpResponseStatus.OK,
                        sendContent);

        response.headers().set(HttpHeaders.Names.CONTENT_TYPE, "application/json");
        response.headers().set(HttpHeaders.Names.CONTENT_LENGTH, response.content().readableBytes());
        response.headers().set(HttpHeaders.Names.CONNECTION, "close");

        ChannelFuture channelFuture = ctx.writeAndFlush(response);
        channelFuture.addListener(ChannelFutureListener.CLOSE);

        request.release();
    }

    private void saveEntity(SendEntity sendEntity, String json) throws UnsupportedEntity {
        switch (sendEntity) {
            case USER:
                Storage.addUser(Parser.parseNewUser(json));
                break;
            case LOCATION:
                Storage.addLocation(Parser.parseNewLocation(json));
                break;
            case VISIT:
                Storage.addVisit(Parser.parseNewVisit(json));
                break;
            default:
                throw new UnsupportedEntity();
        }
    }

    private void updateEntity(SendEntity sendEntity, Long id, String json) throws UnsupportedEntity {
        switch (sendEntity) {
            case USER:
                Parser.parseUpdateUser(json, Storage.getUser(id));
                break;
            case LOCATION:
                Parser.parseUpdateLocation(json, Storage.getLocation(id));
                break;
            case VISIT:
                Parser.parseUpdateVisit(json, Storage.getVisit(id));
                break;
            default:
                throw new UnsupportedEntity();
        }
    }

    private JSONObject selectEntity(SendEntity sendEntity, Long id, QueryStringDecoder reqUri) throws UnsupportedEntity {
        switch (sendEntity) {
            case USER:
                return JsonConverter.getUserJson(Storage.getUser(id));
            case LOCATION:
                return JsonConverter.getLocationJson(Storage.getLocation(id));
            case VISIT:
                return JsonConverter.getVisitJson(Storage.getVisit(id));
            case USER_VISITS:
                Long fromDateVisit = getNullableLong(getParam("fromDate", reqUri.parameters()));
                Long toDateVisit = getNullableLong(getParam("toDate", reqUri.parameters()));
                checkDates(fromDateVisit, toDateVisit);
                return JsonConverter.getUserVisitsJson(
                        Storage.getUser(id),
                        fromDateVisit,
                        toDateVisit,
                        getParam("country", reqUri.parameters()),
                        getNullableLong(getParam("distance", reqUri.parameters()))
                );
            case LOCATION_AVG_MARK:
                Long fromDateMark = getNullableLong(getParam("fromDate", reqUri.parameters()));
                Long toDateMark = getNullableLong(getParam("toDate", reqUri.parameters()));
                checkDates(fromDateMark, toDateMark);
                return JsonConverter.getLocationMarkJson(
                        Storage.getLocation(id),
                        fromDateMark,
                        toDateMark,
                        getAge(getParam("fromDate", reqUri.parameters())),
                        getAge(getParam("toDate", reqUri.parameters())),
                        getGender(getParam("gender", reqUri.parameters()))
                );
            default:
                throw new UnsupportedEntity();
        }
    }

    @Nullable
    private String getParam(@Nonnull String paramName, @Nonnull Map<String, List<String>> params) {
        List<String> paramList = params.get(paramName);
        if (paramList == null) {
            return null;
        }
        if (paramList.isEmpty() || paramList.get(0).isEmpty()) {
            throw new IncorrectParameterException("Wrong parameter - " + paramName);
        }
        return paramList.isEmpty() ? null : paramList.get(0);
    }

    private Long getNullableLong(@Nullable String longStr) {
        if (longStr == null) {
            return null;
        }
        if (StringUtils.isNumeric(longStr)) {
            return Long.getLong(longStr);
        } else {
            throw new IncorrectParameterException("Wrong parameter - " + longStr + " must be Long");
        }
    }

    private int getAge(@Nullable String age) {
        if (age == null) {
            return 0;
        }
        if (StringUtils.isNumeric(age)) {
            return Integer.getInteger(age);
        } else {
            throw new IncorrectParameterException("Wrong parameter - " + age + " must be Long");
        }
    }

    private Boolean getGender(@Nullable String gender) {
        if (gender == null) {
            return null;
        }
        if ("m".equals(gender)) {
            return true;
        } else if ("f".equals(gender)) {
            return false;
        }
        throw new IncorrectParameterException("Wrong gender - " + gender);
    }

    private void checkDates(Long date1, Long date2) {
        if (date1 != null && date2 != null && date1.equals(date2)) {
            throw new IncorrectParameterException("Wrong parameters - equal dates");
        }
    }
}