package com;

import com.entity.Entity;
import com.entity.Location;
import com.entity.User;
import com.entity.Visit;

import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Alexander Yakovlev on 19/08/2017.
 */
public class Storage {
    private static ConcurrentHashMap<Long, User> userMap = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<Long, Location> locationMap = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<Long, Visit> visitMap = new ConcurrentHashMap<>();

    public static void addUser(User user) {
        userMap.put(user.getId(), user);
    }

    public static void addLocation(Location location) {
        locationMap.put(location.getId(), location);
    }

    public static void addVisit(Visit visit) {
        visitMap.put(visit.getId(), visit);
        userMap.get(visit.getUserId()).addUserVisit(visit);
        locationMap.get(visit.getLocationId()).addLocationVisit(visit);
    }

    public static User getUser(Long id) {
        return checkEntity(userMap.get(id));
    }

    public static Location getLocation(Long id) {
        return checkEntity(locationMap.get(id));
    }

    public static Visit getVisit(Long id) {
        return checkEntity(visitMap.get(id));
    }

    private static <E extends Entity> E checkEntity(E entity) {
        if (entity == null) {
            throw new NoSuchElementException("No " + entity.getType() + " with id " + entity.getId());
        }
        return entity;
    }
}
