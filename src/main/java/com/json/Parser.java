package com.json;

import com.entity.Location;
import com.entity.User;
import com.entity.Visit;
import org.json.JSONObject;

/**
 * Created by Alexander Yakovlev on 19/08/2017.
 */
public class Parser {

    //TODO check json exception if null

    public static User parseNewUser(String content) {
        JSONObject userJson = new JSONObject(content);
        Long id = userJson.getLong("id");
        String email = userJson.getString("email");
        String firstName = userJson.getString("first_name");
        String lastName = userJson.getString("last_name");
        boolean gender = "m".equals(userJson.getString("gender"));
        long birthDate = userJson.getLong("birth_date");
        return new User(id, email, firstName, lastName, gender, birthDate);
    }

    public static User parseUpdateUser(String content, User user) {
        JSONObject userJson = new JSONObject(content);
        String email = userJson.getString("email");
        String firstName = userJson.getString("first_name");
        String lastName = userJson.getString("last_name");
        boolean gender = "m".equals(userJson.getString("gender"));
        long birthDate = userJson.getLong("birth_date");
        return user.update(email, firstName, lastName, gender, birthDate);
    }

    public static Location parseNewLocation(String content) {
        JSONObject userJson = new JSONObject(content);
        Long id = userJson.getLong("id");
        String place = userJson.getString("place");
        String country = userJson.getString("country");
        String city = userJson.getString("city");
        long distance = userJson.getLong("distance");
        return new Location(id, place, country, city, distance);
    }

    public static Location parseUpdateLocation(String content, Location location) {
        JSONObject userJson = new JSONObject(content);
        String place = userJson.getString("place");
        String country = userJson.getString("country");
        String city = userJson.getString("city");
        long distance = userJson.getLong("distance");
        return location.update(place, country, city, distance);
    }

    public static Visit parseNewVisit(String content) {
        JSONObject userJson = new JSONObject(content);
        Long id = userJson.getLong("id");
        Long locationId = userJson.getLong("location");
        Long userId = userJson.getLong("user");
        long visitedAt = userJson.getLong("visited_at");
        int mark = userJson.getInt("mark");
        return new Visit(id, locationId, userId, visitedAt, mark);
    }

    public static Visit parseUpdateVisit(String content, Visit visit) {
        JSONObject userJson = new JSONObject(content);
        Long locationId = userJson.getLong("location");
        Long userId = userJson.getLong("user");
        long visitedAt = userJson.getLong("visited_at");
        int mark = userJson.getInt("mark");
        return visit.update(locationId, userId, visitedAt, mark);
    }
}
