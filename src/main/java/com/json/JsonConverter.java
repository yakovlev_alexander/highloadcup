package com.json;

import com.entity.Location;
import com.entity.User;
import com.entity.Visit;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Alexander Yakovlev on 20/08/2017.
 */
public class JsonConverter {

    public static JSONObject getUserJson(User user) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", user.getId());
        jsonObject.put("email", user.getEmail());
        jsonObject.put("first_name", user.getFirstName());
        jsonObject.put("last_name", user.getLastName());
        jsonObject.put("gender", user.isGender() ? "m" : "f");
        jsonObject.put("birth_date", user.getBirthDate());
        return jsonObject;
    }

    public static JSONObject getLocationJson(Location location) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", location.getId());
        jsonObject.put("place", location.getPlace());
        jsonObject.put("country", location.getCountry());
        jsonObject.put("city", location.getCity());
        jsonObject.put("distance", location.getDistance());
        return jsonObject;
    }

    public static JSONObject getVisitJson(Visit visit) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", visit.getId());
        jsonObject.put("location", visit.getLocationId());
        jsonObject.put("user", visit.getUserId());
        jsonObject.put("visited_at", visit.getVisitedAt());
        jsonObject.put("mark", visit.getMark());
        return jsonObject;
    }

    public static JSONObject getUserVisitsJson(User user, Long fromDate, Long toDate, String country, Long distance) {
        JSONArray jsonArray = new JSONArray();

        Map<Visit, Location> userVisits = user.getUserVisits(fromDate, toDate, country, distance);
        for (Map.Entry<Visit, Location> visit : userVisits.entrySet()) {
            JSONObject jsonVisit = new JSONObject();
            jsonVisit.put("mark", visit.getKey().getMark());
            jsonVisit.put("visited_at", visit.getKey().getVisitedAt());
            jsonVisit.put("place", visit.getValue().getPlace());
            jsonArray.put(jsonVisit);
        }

        JSONObject resultJson = new JSONObject();
        resultJson.put("visits", jsonArray);
        return resultJson;
    }

    public static JSONObject getLocationMarkJson(Location location, Long fromDate, Long toDate, int fromAge, int toAge, Boolean gender) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("avg", location.getAvgMark(fromDate, toDate, fromAge, toAge, gender));
        return jsonObject;
    }
}
