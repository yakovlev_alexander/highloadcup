package com;

import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alexander Yakovlev on 20/08/2017.
 */
public class Utils {

    public static final int DOUBLE_PRECISE = 100000;

    public static long getCurrTimeInSec() {
        return System.currentTimeMillis() / 1000;
    }

    public static long currTimePlusYear(int years) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, years);
        return cal.getTimeInMillis() / 1000;
    }


    public static Map<String, String> getQueryMap(String url) {
        String query = url.substring(url.indexOf("?") + 1);

        String[] params = query.split("&");
        Map<String, String> map = new HashMap<>();
        for (String param : params) {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }

    public static void main(String[] args) throws URISyntaxException {
        Map<String, String> queryMap = getQueryMap("/new/val?first=123&sec=qwert");
        System.out.println(queryMap);

        System.out.println("queryMap.get() = " + queryMap.get("first"));
        System.out.println("queryMap.get() = " + queryMap.get("second"));
    }
}
