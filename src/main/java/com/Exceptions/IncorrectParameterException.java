package com.Exceptions;

/**
 * Created by Alexander Yakovlev on 23/08/2017.
 */
public class IncorrectParameterException extends RuntimeException {
    public IncorrectParameterException(String message) {
        super(message);
    }
}
