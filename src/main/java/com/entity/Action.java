package com.entity;

/**
 * Created by Alexander Yakovlev on 19/08/2017.
 */
public enum Action {
    SELECT, CREATE, UPDATE
}
