package com.entity;

import com.Storage;
import com.Utils;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Collectors;

/**
 * Created by Alexander Yakovlev on 19/08/2017.
 */
public class Location implements Entity {

    private Long id;
    private String place;
    private String country;
    private String city;
    private long distance;

    private ConcurrentSkipListMap<Visit, User> visitsByVisitedAt;

    public Location(Long id, String place, String country, String city, long distance) {
        this.id = id;
        this.place = place;
        this.country = country;
        this.city = city;
        this.distance = distance;
        visitsByVisitedAt = new ConcurrentSkipListMap<>();
    }

    public Location update(String place, String country, String city, long distance) {
        this.place = place;
        this.country = country;
        this.city = city;
        this.distance = distance;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getPlace() {
        return place;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public long getDistance() {
        return distance;
    }

    public void addLocationVisit(Visit visit) {
        visitsByVisitedAt.put(visit, Storage.getUser(visit.getUserId()));
    }

    public double getAvgMark(@Nullable Long fromDate, @Nullable Long toDate, int fromAge, int toAge, @Nullable Boolean gender) {
        Map<Visit, User> visits = fromDate != null && toDate != null
                ? visitsByVisitedAt.subMap(Visit.getComparableVisit(fromDate), Visit.getComparableVisit(toDate))
                : visitsByVisitedAt;

        if (fromAge > 0 && toAge > 0) {
            final long bottomTimestamp = Utils.currTimePlusYear(-fromAge);
            final long upTimestamp = Utils.currTimePlusYear(-toAge);
            visits.entrySet().removeIf(entry -> bottomTimestamp < entry.getValue().getBirthDate() || entry.getValue().getBirthDate() < upTimestamp);
        }

        if (gender != null) {
            visits.entrySet().removeIf(entry -> entry.getValue().isGender() != gender);
        }

        double res = visits.entrySet()
                .stream()
                .collect(Collectors.averagingDouble(entry -> entry.getKey().getMark()));

        res = res * Utils.DOUBLE_PRECISE;
        int i = (int) Math.round(res);
        return (double) i / Utils.DOUBLE_PRECISE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        return id.equals(location.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
