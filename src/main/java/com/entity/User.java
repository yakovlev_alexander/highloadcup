package com.entity;

import com.Storage;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Created by Alexander Yakovlev on 19/08/2017.
 */
public class User implements Comparable<User>, Entity {

    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private boolean gender;
    private long birthDate;

    private ConcurrentMap<String, ConcurrentSkipListMap<Visit, Location>> visitsByLocationCountry;

    private ConcurrentSkipListMap<Visit, Location> visitsByVisitedAt;

    public User(Long id, String email, String firstName, String lastName, boolean gender, long birthDate) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthDate = birthDate;
        this.visitsByLocationCountry = new ConcurrentHashMap<>();
        this.visitsByVisitedAt = new ConcurrentSkipListMap<>();
    }

    public User update(String email, String firstName, String lastName, boolean gender, long birthDate) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthDate = birthDate;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean isGender() {
        return gender;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void addUserVisit(Visit visit) {
        ConcurrentSkipListMap<Visit, Location> visits = visitsByLocationCountry.getOrDefault(visit.getLocationId(), new ConcurrentSkipListMap<>());
        Location location = Storage.getLocation(visit.getLocationId());
        visits.put(visit, location);
        visitsByLocationCountry.putIfAbsent(location.getCountry(), visits);

        visitsByVisitedAt.put(visit, location);
    }

    public Map<Visit, Location> getUserVisits(@Nullable Long fromDate, @Nullable Long toDate, @Nullable String country, @Nullable Long distance) {
        ConcurrentSkipListMap<Visit, Location> visits = country != null
                ? visitsByLocationCountry.get(country)
                : visitsByVisitedAt;

        Map<Visit, Location> returnMap = fromDate != null && toDate != null
                ? visits.subMap(Visit.getComparableVisit(fromDate), Visit.getComparableVisit(toDate))
                : visits;

        if (distance != null) {
            returnMap.entrySet().removeIf(entry -> entry.getValue().getDistance() > distance);
        }

        return returnMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender=" + gender +
                ", birthDate=" + birthDate +
                '}';
    }

    @Override
    public int compareTo(User o) {
        return 0;
    }
}
