package com.entity;

/**
 * Created by Alexander Yakovlev on 19/08/2017.
 */
public class Visit implements Comparable<Visit>, Entity {

    private Long id;
    private Long locationId;
    private Long userId;
    private long visitedAt;
    private int mark;

    public Visit(Long id, Long locationId, Long userId, long visitedAt, int mark) {
        this.id = id;
        this.locationId = locationId;
        this.userId = userId;
        this.visitedAt = visitedAt;
        this.mark = mark;
    }

    private Visit(long visitedAt) {
        this.visitedAt = visitedAt;
    }

    public Visit update(Long locationId, Long userId, long visitedAt, int mark) {
        this.locationId = locationId;
        this.userId = userId;
        this.visitedAt = visitedAt;
        this.mark = mark;
        return this;
    }

    @Override
    public Long getId() {
        return id;
    }

    public Long getLocationId() {
        return locationId;
    }

    public Long getUserId() {
        return userId;
    }

    public long getVisitedAt() {
        return visitedAt;
    }

    public int getMark() {
        return mark;
    }

    public static Visit getComparableVisit(long visitedAt) {
        return new Visit(visitedAt);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Visit visit = (Visit) o;

        return id.equals(visit.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }


    @Override
    public int compareTo(Visit o) {
        return o.visitedAt > this.visitedAt ? -1 : o.visitedAt < this.visitedAt ? 1 : 0;
    }
}
