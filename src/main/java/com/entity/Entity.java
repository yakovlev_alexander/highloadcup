package com.entity;

/**
 * Created by Alexander Yakovlev on 23/08/2017.
 */
public interface Entity {
    Long getId();

    default String getType() {
        return getClass().getTypeName();
    }
}
