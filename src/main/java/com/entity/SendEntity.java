package com.entity;

/**
 * Created by Alexander Yakovlev on 19/08/2017.
 */
public enum SendEntity {
    USER, LOCATION, VISIT, LOCATION_AVG_MARK, USER_VISITS
}
