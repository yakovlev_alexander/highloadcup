# Наследуемся от Oracle java
FROM jtech/oracle-jdk:8u131

# Создаем папку для проекта
RUN mkdir hlcup

# Копируем джарник внутрь контейнера
ADD target/highload-cup-1.0-SNAPSHOT-jar-with-dependencies.jar hlcup



# Открываем 80-й порт наружу
EXPOSE 80

# Запускаем наш сервер
CMD java -Xmx1024m -Xms1024m -jar ./hlcup/highload-cup-1.0-SNAPSHOT-jar-with-dependencies.jar